
<?php
session_start();
$contador = 0;



$aspirantes[][] = array();

if(isset ($_SESSION["aspirantes"]) ) {
  //si existe pasamos su valor a la varible local
 $aspirantes = $_SESSION["aspirantes"];
 $contador = $_SESSION["contador"];
 $indiceEdicion = $_SESSION["indiceEdicion"];
 }

if(!isset ($_SESSION["aspirantes"]) ) {
 //si no existe la creamos con el valor inicial
 $_SESSION["indiceEdicion"] = 0;
 $_SESSION["aspirantes"] = array();
 $aspirantes[0][0] = "Sin datos";
 $aspirantes[0][1] = "Sin datos";
 $aspirantes[0][2] = "Sin datos";
 $aspirantes[0][3] = "Sin datos";
 $aspirantes[0][4] = "Sin datos";
 $_SESSION["contador"] = 0;
}
if (isset($_POST['nombre']))
{

  $aspirantes[$contador][0] = $_POST["nombre"];
  $aspirantes[$contador][1] = $_POST["apellido"];
  $aspirantes[$contador][2] = $_POST["ciudad"];
  $aspirantes[$contador][3] = $_POST["edad"];
  $aspirantes[$contador][4] = $_POST["ingles"];
  $_SESSION["contador"] = $contador + 1;
  $_SESSION["aspirantes"] = $aspirantes;
  unset($_POST["nombre"]);
}
if (isset($_POST['nombre_editado']))
{
  $aspirantes[$indiceEdicion][0] = $_POST["nombre_editado"];
  $aspirantes[$indiceEdicion][1] = $_POST["apellido_editado"];
  $aspirantes[$indiceEdicion][2] = $_POST["ciudad_editado"];
  $aspirantes[$indiceEdicion][3] = $_POST["edad_editado"];
  $aspirantes[$indiceEdicion][4] = $_POST["ingles_editado"];
  $_SESSION["aspirantes"] = $aspirantes;
  
  unset($_POST);
}

if (isset($_POST['borrar']))
{
  $indice = $_POST['borrar'];
  array_splice($aspirantes, $indice, 1);
  $_SESSION["aspirantes"] = $aspirantes;

  if($_SESSION["contador"] <= 0){
    $contador = 0;
  }else{
    $_SESSION["contador"] = $contador - 1;
  }
  
  unset($_POST);
}

?>



 <?php
if (isset($_POST['editar'])):
  $indiceEdicion = $_POST['editar'];
  $_SESSION["indiceEdicion"] = $indiceEdicion;
//CARGAR SCRIPT DEL MODAL EDITAR Y EJECUTARLO SI EXISTE LA VS $_POST["EDITAR"]
?>
<script src="build/js/jquery-3.4.1.js"></script>
<script src="build/js/modal2.js"> </script>
<?php

unset($_POST["editar"]);
endif;
?>





<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRUD AspiranteCallCenterBPO</title>
    <link rel="stylesheet" href="build/css/app.css">
    <link rel="stylesheet" href="build/css/style-ir.css">
</head>
<body>

  
    
    <div class="bg-gray-100 font-sans w-full min-h-screen m-0">
	<div class="bg-white shadow">
    <div class="container mx-auto px-4">
        
      <div class="flex items-center justify-between py-4">
        <div>
          <svg xmlns="http://www.w3.org/2000/svg" class="w-10 h-10 text-purple-600" viewBox="0 0 24 24">
            <path fill="currentColor" d="M14.5,16 C14.2238576,16 14,15.7761424 14,15.5 L14,9.5 C14,9.22385763 14.2238576,9 14.5,9 L16,9 C17.1045695,9 18,9.8954305 18,11 C18,11.4116588 17.8756286,11.7942691 17.6624114,12.1123052 C18.4414283,12.3856578 19,13.1275982 19,14 C19,15.1045695 18.1045695,16 17,16 L14.5,16 Z M15,15 L17,15 C17.5522847,15 18,14.5522847 18,14 C18,13.4477153 17.5522847,13 17,13 L15,13 L15,15 Z M15,12 L16,12 C16.5522847,12 17,11.5522847 17,11 C17,10.4477153 16.5522847,10 16,10 L15,10 L15,12 Z M12.9499909,4 L19.5,4 C20.8807119,4 22,5.11928813 22,6.5 L22,19.5 C22,20.8807119 20.8807119,22 19.5,22 L13.5,22 C12.2700325,22 11.2475211,21.1117749 11.0389093,19.9417682 C10.8653433,19.9799013 10.6850188,20 10.5,20 L4.5,20 C3.11928813,20 2,18.8807119 2,17.5 L2,4.5 C2,3.11928813 3.11928813,2 4.5,2 L10.5,2 C11.709479,2 12.7183558,2.85887984 12.9499909,4 Z M13,5 L13,17.5 C13,18.3179089 12.6072234,19.0440799 12,19.5001831 C12.0000989,20.3285261 12.6716339,21 13.5,21 L19.5,21 C20.3284271,21 21,20.3284271 21,19.5 L21,6.5 C21,5.67157288 20.3284271,5 19.5,5 L13,5 Z M8.56005566,11.4964303 C8.54036595,11.4987873 8.52032459,11.5 8.5,11.5 L6.5,11.5 C6.47967541,11.5 6.45963405,11.4987873 6.43994434,11.4964303 L5.96423835,12.6856953 C5.86168164,12.9420871 5.57069642,13.066795 5.31430466,12.9642383 C5.0579129,12.8616816 4.93320495,12.5706964 5.03576165,12.3143047 L7.03576165,7.31430466 C7.20339081,6.89523178 7.79660919,6.89523178 7.96423835,7.31430466 L9.96423835,12.3143047 C10.066795,12.5706964 9.9420871,12.8616816 9.68569534,12.9642383 C9.42930358,13.066795 9.13831836,12.9420871 9.03576165,12.6856953 L8.56005566,11.4964303 Z M8.16148352,10.5 L7.5,8.8462912 L6.83851648,10.5 L8.16148352,10.5 Z M10.5,3 L4.5,3 C3.67157288,3 3,3.67157288 3,4.5 L3,17.5 C3,18.3284271 3.67157288,19 4.5,19 L10.5,19 C11.3284271,19 12,18.3284271 12,17.5 L12,4.5 C12,3.67157288 11.3284271,3 10.5,3 Z M6.5,18 C6.22385763,18 6,17.7761424 6,17.5 C6,17.2238576 6.22385763,17 6.5,17 L8.5,17 C8.77614237,17 9,17.2238576 9,17.5 C9,17.7761424 8.77614237,18 8.5,18 L6.5,18 Z M15.5,20 C15.2238576,20 15,19.7761424 15,19.5 C15,19.2238576 15.2238576,19 15.5,19 L17.5,19 C17.7761424,19 18,19.2238576 18,19.5 C18,19.7761424 17.7761424,20 17.5,20 L15.5,20 Z"/>
          </svg>
        </div>

        <div class="hidden sm:flex sm:items-center">

          <a href="#" class="text-gray-800 text-sm font-semibold hover:text-purple-600 ml-12">CCBPO CRUD 1.0</a>

        </div>


  
        <div class=" sm:flex sm:items-center">
          <a  href="#" class="modal-open3 text-gray-800 text-sm font-semibold border px-4 py-2 rounded-lg hover:text-purple-600 hover:border-purple-600">Buscar Aspirante</a>
        </div>
        <div class=" sm:flex sm:items-center">
          <a  href="#" class="modal-open text-gray-800 text-sm font-semibold border px-4 py-2 rounded-lg hover:text-purple-600 hover:border-purple-600">Agregar nuevo aspirante</a>
        </div>

        <div class="sm:hidden cursor-pointer">
          <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 text-purple-600" viewBox="0 0 24 24">
            <path fill="currentColor" d="M12.9499909,17 C12.7183558,18.1411202 11.709479,19 10.5,19 C9.29052104,19 8.28164422,18.1411202 8.05000906,17 L3.5,17 C3.22385763,17 3,16.7761424 3,16.5 C3,16.2238576 3.22385763,16 3.5,16 L8.05000906,16 C8.28164422,14.8588798 9.29052104,14 10.5,14 C11.709479,14 12.7183558,14.8588798 12.9499909,16 L20.5,16 C20.7761424,16 21,16.2238576 21,16.5 C21,16.7761424 20.7761424,17 20.5,17 L12.9499909,17 Z M18.9499909,12 C18.7183558,13.1411202 17.709479,14 16.5,14 C15.290521,14 14.2816442,13.1411202 14.0500091,12 L3.5,12 C3.22385763,12 3,11.7761424 3,11.5 C3,11.2238576 3.22385763,11 3.5,11 L14.0500091,11 C14.2816442,9.85887984 15.290521,9 16.5,9 C17.709479,9 18.7183558,9.85887984 18.9499909,11 L20.5,11 C20.7761424,11 21,11.2238576 21,11.5 C21,11.7761424 20.7761424,12 20.5,12 L18.9499909,12 Z M9.94999094,7 C9.71835578,8.14112016 8.70947896,9 7.5,9 C6.29052104,9 5.28164422,8.14112016 5.05000906,7 L3.5,7 C3.22385763,7 3,6.77614237 3,6.5 C3,6.22385763 3.22385763,6 3.5,6 L5.05000906,6 C5.28164422,4.85887984 6.29052104,4 7.5,4 C8.70947896,4 9.71835578,4.85887984 9.94999094,6 L20.5,6 C20.7761424,6 21,6.22385763 21,6.5 C21,6.77614237 20.7761424,7 20.5,7 L9.94999094,7 Z M7.5,8 C8.32842712,8 9,7.32842712 9,6.5 C9,5.67157288 8.32842712,5 7.5,5 C6.67157288,5 6,5.67157288 6,6.5 C6,7.32842712 6.67157288,8 7.5,8 Z M16.5,13 C17.3284271,13 18,12.3284271 18,11.5 C18,10.6715729 17.3284271,10 16.5,10 C15.6715729,10 15,10.6715729 15,11.5 C15,12.3284271 15.6715729,13 16.5,13 Z M10.5,18 C11.3284271,18 12,17.3284271 12,16.5 C12,15.6715729 11.3284271,15 10.5,15 C9.67157288,15 9,15.6715729 9,16.5 C9,17.3284271 9.67157288,18 10.5,18 Z"/>
          </svg>
        </div>
      </div>
      

    </div>
    
  </div> 
  
  <div class="md:px-32 py-8 w-full">
  <div class="shadow overflow-scroll sm:overflow-hidden rounded border-b border-gray-200">
      <table class="min-w-full bg-white">
      <thead class="bg-gray-800 text-white">
      <tr>

    <th class="w-1/5 text-center py-3 px-4 uppercase font-semibold text-sm">Nombres</th>
    <th class="w-1/5 text-center py-3 px-4 uppercase font-semibold text-sm">Apellidos</th>
    <th class="w-1/5 text-center py-3 px-4 uppercase font-semibold text-sm">Ciudad</th>
    <th class="w-1/5 text-center py-3 px-4 uppercase font-semibold text-sm">Edad</th>
    <th class="w-1/5 text-center py-3 px-4 uppercase font-semibold text-sm">Ingles</th>
    <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Acciones</th>
    </tr>
    </thead>
    
  <tbody class="text-gray-700">
  
  <?php

    for ($i=0; $i < count($aspirantes); $i++) { 
      echo '<tr>';
      for ($y=0; $y < 5; $y++) { 
        echo'<td class="w-1/3 text-center py-3 px-4">'.$aspirantes[$i][$y].'</td>'; 
      } 
      echo'<td class="p-3 px-5 flex justify-end">
      <form method="post">
      <button name="editar" id="editar"
      type="submit" 
      class="mr-3 text-sm bg-blue-500 hover:bg-blue-700 text-white py-1 
          px-2 rounded focus:outline-none focus:shadow-outline cc_pointer" 
          value='.$i.'>Editar</button>
      <button name="borrar" type="submit" class="text-sm bg-red-500 hover:bg-red-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline" value='.$i.'>Borrar</button>
      </form>';
      echo'</td>';
      echo'</tr>';
    }
    
  ?>
  

  
        


</div>
<div class="modal3 opacity-0 pointer-events-none fixed w-full h-full top-0 left-0 flex items-center justify-center">
  <div class="modal-overlay3 absolute w-full h-full bg-gray-900 opacity-50"></div>
  
  <div class="modal-container bg-white w-11/12 md:max-w-md mx-auto rounded shadow-lg z-50 overflow-y-auto">
    
    <div class="modal-close3 absolute top-0 right-0 cursor-pointer flex flex-col items-center mt-4 mr-4 text-white text-sm z-50">
      <svg class="fill-current text-white" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
        <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"></path>
      </svg>
      <span class="text-sm">(Esc)</span>
    </div>
  
    <div class="modal-content py-4 text-left px-6">
      <!--Title-->
      <div class="flex justify-between items-center pb-3">
        <p class="text-2xl font-bold">Buscar Aspirante</p>
        <div class="modal-close3 cursor-pointer z-50">
          <svg class="fill-current text-black" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
            <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"></path>
          </svg>
        </div>
      </div>
  
      <!--NUEVO ASPIRANTE-->
        <form method="post">
          <div class="flex pr-4">
          <span class="text-sm border border-2 rounded-l px-4 py-2 bg-gray-300 whitespace-no-wrap">Buscar por Nombre:</span>
          <input name="buscar" class="border border-2 rounded-r px-4 py-2 w-full" type="text" placeholder="Escribe el nombre aqui" required/>
          </div><br>  

          <!--Footer-->
          <div class="flex justify-end pt-2">
            <button type="submit" class="mr-3 text-sm bg-blue-500 hover:bg-blue-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline cc_pointer">Buscar</button>
            <button type="button"class="modal-close3 text-sm bg-red-500 hover:bg-red-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline">Cerrar</button>
          </div>
  
        </form>
    </div>
  
  </div>
  </div>

  <div class="modal4 opacity-0 pointer-events-none fixed w-full h-full top-0 left-0 flex items-center justify-center">
  <div class="modal-overlay4 absolute w-full h-full bg-gray-900 opacity-50"></div>
  
  <div class="modal-container bg-white w-11/12 md:max-w-md mx-auto rounded shadow-lg z-50 overflow-y-auto">
    
    <div class="modal-close4 absolute top-0 right-0 cursor-pointer flex flex-col items-center mt-4 mr-4 text-white text-sm z-50">
      <svg class="fill-current text-white" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
        <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"></path>
      </svg>
      <span class="text-sm">(Esc)</span>
    </div>
  
    <div class="modal-content py-4 text-left px-6">
      <!--Title-->
      <div class="flex justify-between items-center pb-3">
        <p class="text-2xl font-bold">Resulado de busqueda:</p>
        <div class="modal-close4 cursor-pointer z-50">
          <svg class="fill-current text-black" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
            <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"></path>
          </svg>
        </div>
      </div>
  
      <!--NUEVO ASPIRANTE-->
        <form method="post">
        <?php
        if(isset($_POST["buscar"])){
          for ($i=0; $i < count($aspirantes); $i++) { 
            for ($y=0; $y < 5; $y++) { 
              if($aspirantes[$i][$y] == $_POST["buscar"]){
                echo'<div class="flex pr-4">
                <span class="text-sm border border-2 rounded-l px-4 py-2 bg-gray-300 whitespace-no-wrap">Nombre:</span>
                <input name="nombre_buscar"  class="border border-2 rounded-r px-4 py-2 w-full" type="text" placeholder="Escribe tu primer nombre" value="'.$aspirantes[$i][0].'" required/>
                </div><br>  
                <div class="flex pr-4">
                <span class="text-sm border border-2 rounded-l px-4 py-2 bg-gray-300 whitespace-no-wrap">Apellidos:</span>
                <input name="apellido_buscar" class="border border-2 rounded-r px-4 py-2 w-full" type="text" placeholder="Escribe tus apellidos" value="'.$aspirantes[$i][1].'" required/>
                </div><br>  
                <div class="flex pr-4">
                <span class="text-sm border border-2 rounded-l px-4 py-2 bg-gray-300 whitespace-no-wrap">Ciudad:</span>
                <input name="ciudad_buscar" class="border border-2 rounded-r px-4 py-2 w-full" type="text" placeholder="¿De que ciudad eres?" value="'.$aspirantes[$i][2].'" required/>
                </div><br>  
                <div class="flex pr-4">
                <span class="text-sm border border-2 rounded-l px-4 py-2 bg-gray-300 whitespace-no-wrap">Edad:</span>
                <input name="edad_buscar" class="border border-2 rounded-r px-4 py-2 w-full" type="text" placeholder="¿Que edad tienes?" value="'.$aspirantes[$i][3].'" required/>
                </div><br>  
                <div class="flex pr-4">
                <span class="text-sm border border-2 rounded-l px-4 py-2 bg-gray-300 whitespace-no-wrap">Ingles:</span>
                <input name="ingles_buscar" class="border border-2 rounded-r px-4 py-2 w-full" type="text" placeholder="¿Que nivel de ingles eres?" value="'.$aspirantes[$i][4].'" required/>
                </div>';
              }
            }     
          }
        }
        

        ?>
          
        
          <!--Footer-->
          <div class="flex justify-end pt-2">
            <button type="submit"class="text-sm bg-red-500 hover:bg-red-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline">Cerrar</button>
          </div>
  
        </form>
    </div>
  
  </div>
  </div>
  <!--Modal Para Editar Nuevo Aspirante-->

 <div class=" modal opacity-0 pointer-events-none fixed w-full h-full top-0 left-0 flex items-center justify-center">
  <div class="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"></div>
  
  <div class="modal-container bg-white w-11/12 md:max-w-md mx-auto rounded shadow-lg z-50 overflow-y-auto">
    
    <div class="modal-close absolute top-0 right-0 cursor-pointer flex flex-col items-center mt-4 mr-4 text-white text-sm z-50">
      <svg class="fill-current text-white" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
        <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"></path>
      </svg>
      <span class="text-sm">(Esc)</span>
    </div>
  
    <div class="modal-content py-4 text-left px-6">
      <!--Title-->
      <div class="flex justify-between items-center pb-3">
        <p class="text-2xl font-bold">Agregar nuevo aspirante</p>
        <div class="modal-close cursor-pointer z-50">
          <svg class="fill-current text-black" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
            <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"></path>
          </svg>
        </div>
      </div>
  
      <!--NUEVO ASPIRANTE-->
        <form method="post">
          <div class="flex pr-4">
          <span class="text-sm border border-2 rounded-l px-4 py-2 bg-gray-300 whitespace-no-wrap">Nombre:</span>
          <input name="nombre"  class="border border-2 rounded-r px-4 py-2 w-full" type="text" placeholder="Escribe tu primer nombre" required/>
          </div><br>  
          <div class="flex pr-4">
          <span class="text-sm border border-2 rounded-l px-4 py-2 bg-gray-300 whitespace-no-wrap">Apellidos:</span>
          <input name="apellido" class="border border-2 rounded-r px-4 py-2 w-full" type="text" placeholder="Escribe tus apellidos" required/>
          </div><br>  
          <div class="flex pr-4">
          <span class="text-sm border border-2 rounded-l px-4 py-2 bg-gray-300 whitespace-no-wrap">Ciudad:</span>
          <input name="ciudad" class="border border-2 rounded-r px-4 py-2 w-full" type="text" placeholder="¿De que ciudad eres?" required/>
          </div><br>  
          <div class="flex pr-4">
          <span class="text-sm border border-2 rounded-l px-4 py-2 bg-gray-300 whitespace-no-wrap">Edad:</span>
          <input name="edad" class="border border-2 rounded-r px-4 py-2 w-full" type="text" placeholder="¿Que edad tienes?" required/>
          </div><br>  
          <div class="flex pr-4">
          <span class="text-sm border border-2 rounded-l px-4 py-2 bg-gray-300 whitespace-no-wrap">Ingles:</span>
          <input name="ingles" class="border border-2 rounded-r px-4 py-2 w-full" type="text" placeholder="¿Que nivel de ingles eres?" required/>
          </div>
        
          <!--Footer-->
          <div class="flex justify-end pt-2">
            <button type="submit" class="mr-3 text-sm bg-blue-500 hover:bg-blue-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline cc_pointer">Guardar</button>
            <button type="button"class="modal-close text-sm bg-red-500 hover:bg-red-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline">Cerrar</button>
          </div>
  
        </form>
    </div>
  
  </div>
  </div>

  <div class="modal2 opacity-0 pointer-events-none fixed w-full h-full top-0 left-0 flex items-center justify-center">
  <div class="modal-overlay2 absolute w-full h-full bg-gray-900 opacity-50"></div>
  
<div class="modal-container bg-white w-11/12 md:max-w-md mx-auto rounded shadow-lg z-50 overflow-y-auto">
    
    <div class="modal-close2 absolute top-0 right-0 cursor-pointer flex flex-col items-center mt-4 mr-4 text-white text-sm z-50">
      <svg class="fill-current text-white" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
        <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"></path>
      </svg>
      <span class="text-sm">(Esc)</span>
    </div>
  
    <div class="modal-content py-4 text-left px-6">
      <!--Title-->
      <div class="flex justify-between items-center pb-3">
        <p class="text-2xl font-bold">Editar aspirante</p>
        <div class="modal-close2 cursor-pointer z-50">
          <svg class="fill-current text-black" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
            <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"></path>
          </svg>
        </div>
      </div>
      <?php

      echo'<form method="post">
      <div class="flex pr-4">
      <span class="text-sm border border-2 rounded-l px-4 py-2 bg-gray-300 whitespace-no-wrap">Nombre:</span>
      <input name="nombre_editado" class="user border border-2 rounded-r px-4 py-2 w-full" type="text" placeholder="Escribe tu nombre" value="'.$aspirantes[$indiceEdicion][0].'" required />
      </div><br>  
      <div class="flex pr-4">
      <span class="text-sm border border-2 rounded-l px-4 py-2 bg-gray-300 whitespace-no-wrap">Apellidos:</span>
      <input name="apellido_editado" class="border border-2 rounded-r px-4 py-2 w-full" type="text" placeholder="Escribe tus apellidos" value="'.$aspirantes[$indiceEdicion][1].'" required />
      </div><br>  
      <div class="flex pr-4">
      <span class="text-sm border border-2 rounded-l px-4 py-2 bg-gray-300 whitespace-no-wrap">Ciudad:</span>
      <input name="ciudad_editado" class="border border-2 rounded-r px-4 py-2 w-full" type="text" placeholder="¿De que ciudad eres?" value="'.$aspirantes[$indiceEdicion][2].'" required/>
      </div><br>  
      <div class="flex pr-4">
      <span class="text-sm border border-2 rounded-l px-4 py-2 bg-gray-300 whitespace-no-wrap">Edad:</span>
      <input name="edad_editado" class="border border-2 rounded-r px-4 py-2 w-full" type="text" placeholder="¿Que edad tienes?" value="'.$aspirantes[$indiceEdicion][3].'" requiered />
      </div><br>  
      <div class="flex pr-4">
      <span class="text-sm border border-2 rounded-l px-4 py-2 bg-gray-300 whitespace-no-wrap">Ingles:</span>
      <input name="ingles_editado" class="border border-2 rounded-r px-4 py-2 w-full" type="text" placeholder="¿Que nivel de ingles eres?" value="'.$aspirantes[$indiceEdicion][4].'" required/>
      </div>';
      
       ?>
      <!--EDITAR ASPIRANTE-->
        
        
          <!--Footer-->
          <div class="flex justify-end pt-2">
            <button type="submit" class="mr-3 text-sm bg-blue-500 hover:bg-blue-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline cc_pointer">Guardar</button>
            <button type="button" class="modal-close2 text-sm bg-red-500 hover:bg-red-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline">Cerrar</button>
          </div>
  
        </form>
    </div>
  
  </div>
  
  </div>
  <div class="font-mono text-lg text-gray-800 text-center mr-12">
  <a href="logout.php">Reiniciar Sesion</a>
  </div>
</body>
<script src="build/js/app.js"></script>
<script src="build/js/modal.js"></script>
<script src="build/js/jquery-3.4.1.js"></script>
<?php
if (isset($_POST['buscar'])):
//CARGAR SCRIPT DEL MODAL EDITAR Y EJECUTARLO SI EXISTE LA VS $_POST["EDITAR"]
?>
<script src="build/js/jquery-3.4.1.js"></script>
<script src="build/js/modal4.js"> </script>
<?php
unset($_POST["buscar"]);
endif;
?>

</html>
