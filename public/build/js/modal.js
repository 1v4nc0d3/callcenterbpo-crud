window.onload = function() {
    //AQUI SE EJECUTA EL MODAL CON LA CLASE .MODAL-OPEN
    var openmodal = document.querySelectorAll('.modal-open')
    for (var i = 0; i < openmodal.length; i++) {
      openmodal[i].addEventListener('click', function(event){
    	event.preventDefault()
    	toggleModal()
      })
    }
    
    const overlay = document.querySelector('.modal-overlay')
    overlay.addEventListener('click', toggleModal)
    
    var closemodal = document.querySelectorAll('.modal-close')
    for (var i = 0; i < closemodal.length; i++) {
      closemodal[i].addEventListener('click', toggleModal)
    }
    
    document.onkeydown = function(evt) {
      evt = evt || window.event
      var isEscape = false
      if ("key" in evt) {
    	isEscape = (evt.key === "Escape" || evt.key === "Esc")
      } else {
    	isEscape = (evt.keyCode === 27)
      }
      if (isEscape && document.body.classList.contains('modal-active')) {
    	toggleModal()
      }
    };
    
    
    function toggleModal () {
      const body = document.querySelector('body')
 
        const modal = document.querySelector('.modal')
        modal.classList.toggle('opacity-0')
        modal.classList.toggle('pointer-events-none')
        body.classList.toggle('modal-active')
    
    }

    var openmodal3 = document.querySelectorAll('.modal-open3')
    for (var i = 0; i < openmodal3.length; i++) {
      openmodal3[i].addEventListener('click', function(event){
    	event.preventDefault()
    	toggleModal3()
      })
    }
    
    const overlay3 = document.querySelector('.modal-overlay3')
    overlay3.addEventListener('click', toggleModal3)
    
    var closemodal3 = document.querySelectorAll('.modal-close3')
    for (var i = 0; i < closemodal.length; i++) {
      closemodal3[i].addEventListener('click', toggleModal3)
    }
    
    document.onkeydown = function(evt) {
      evt = evt || window.event
      var isEscape = false
      if ("key" in evt) {
    	isEscape = (evt.key === "Escape" || evt.key === "Esc")
      } else {
    	isEscape = (evt.keyCode === 27)
      }
      if (isEscape && document.body.classList.contains('modal-active3')) {
    	toggleModal3()
      }
    };
    
    
    function toggleModal3 () {
      const body = document.querySelector('body')
 
        const modal3 = document.querySelector('.modal3')
        modal3.classList.toggle('opacity-0')
        modal3.classList.toggle('pointer-events-none')
        body.classList.toggle('modal-active3')
    
    }
    
  }
  


