$(document).ready(function(){
    var openmodal2 = document.querySelectorAll('.modal-open2')
    for (var i = 0; i < openmodal2.length; i++) {
      openmodal2[i].addEventListener('click', function(event){
      event.preventDefault()
      toggleModal2()
      })
    }
    
    const overlay2 = document.querySelector('.modal-overlay2')
    overlay2.addEventListener('click', toggleModal2)
    
    var closemodal2 = document.querySelectorAll('.modal-close2')
    for (var i = 0; i < closemodal2.length; i++) {
      closemodal2[i].addEventListener('click', toggleModal2)
    }
    
    document.onkeydown = function(evt2) {
      evt2 = evt2 || window.event
      var isEscape = false
      if ("key" in evt2) {
      isEscape = (evt2.key === "Escape" || evt2.key === "Esc")
      } else {
      isEscape = (evt2.keyCode === 27)
      }
      if (isEscape && document.body.classList.contains('modal-active2')) {
      toggleModal2()
      }
    };
    
    
    function toggleModal2 () {
      const body2 = document.querySelector('body')
    
        const modal2 = document.querySelector('.modal2')
        modal2.classList.toggle('opacity-0')
        modal2.classList.toggle('pointer-events-none')
        body2.classList.toggle('modal-active2')
    
    }
  toggleModal2();
});
