$(document).ready(function(){
    var openmodal4 = document.querySelectorAll('.modal-open4')
    for (var i = 0; i < openmodal4.length; i++) {
      openmodal4[i].addEventListener('click', function(event){
      event.preventDefault()
      toggleModal4()
      })
    }
    
    const overlay4 = document.querySelector('.modal-overlay4')
    overlay4.addEventListener('click', toggleModal4)
    
    var closemodal4 = document.querySelectorAll('.modal-close4')
    for (var i = 0; i < closemodal4.length; i++) {
      closemodal4[i].addEventListener('click', toggleModal4)
    }
    
    document.onkeydown = function(evt4) {
      evt4 = evt4 || window.event
      var isEscape = false
      if ("key" in evt4) {
      isEscape = (evt4.key === "Escape" || evt4.key === "Esc")
      } else {
      isEscape = (evt4.keyCode === 27)
      }
      if (isEscape && document.body.classList.contains('modal-active4')) {
      toggleModal4()
      }
    };
    
    
    function toggleModal4 () {
      const body4 = document.querySelector('body')
    
        const modal4 = document.querySelector('.modal4')
        modal4.classList.toggle('opacity-0')
        modal4.classList.toggle('pointer-events-none')
        body4.classList.toggle('modal-active4')
    
    }
  toggleModal4();
});
